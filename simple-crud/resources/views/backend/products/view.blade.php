<x-backend.layouts.master>
    <div class="container">
        <div class="card">

            <div class="card-body">
                <div class="breadcrumb">
                    <li><a href="{{ route('product.index') }}">Products</a></li>/
                    <li>{{$product->title}}</li>
                </div>
            </div>



        </div>

          
            <div class="card" style="max-width: 350px" >
                <div class="text-center">
                    <img src="{{asset('storage/products/'.$product->image)}}" class="img-fluid rounded" style="width:fit-content;" alt="No image">
                </div>
                <div class="card-title text-center">
                    {{$product->title}}
                    <hr>
                </div>
                <div class="card-body">

                    <div class="card-group row w-100">
                        <div class="card-column">
                            <p>{{$product->description}}</p>
                        </div>
                        <div class="col">
                            Price: {{$product->price}} <br>
                         Size: <mark>  {{$product->size}} </mark> 
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex"><a href="{{route('product.index')}}" role="button" class="btn btn-outline-success w-50" >Back</a>
                        <a href="{{route('product.edit',$product->id)}}"  class="btn btn-outline-info w-50">Edit</a></div>
                    
                    <form action="" method="POST">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-danger w-100">Delete</button>
                    </form>
                </div>
            </div>
       
    </div>
</x-backend.layouts.master>