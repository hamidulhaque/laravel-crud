<x-backend.layouts.master>

    <div class="container">
        <div class="row">
            <div class="card">

                <div class="card-body">
                    <div class="breadcrumb">
                        <li><a href="{{ route('product.index') }}">Products</a></li>/
                        <li> Edit</li>/
                        <li><a href=""></a></li>
                    </div>
                </div>

            </div>

            <div class="card">
                <div class="card-title">
                    <legend class="text-center">Edit Product</legend>
                </div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                    @endif                                                        
                    <form action="{{ route('product.update',$product->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <!-- 2 column grid layout with text inputs for the first and last names -->
                        <div class="row">
                            <div class="col">
                                <div class="form-outline">
                                    <input type="text" id="form6Example1" class="form-control" name="title"
                                        value="{{ old('title',$product->title) }}" />
                                    <label class="form-label" for="form6Example1">Product title</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-outline">
                                    <input type="number" id="form6Example5" class="form-control" name="price"
                                        value="{{ old('price', $product->price) }}" />
                                    <label class="form-label" for="form6Example5">Price</label>
                                </div>
                            </div>
                        </div>

                        <!-- Text input -->


                        <!-- Text input -->



                        <!-- Number input -->


                        <!-- Message input -->
                        <div class="form-outline">
                            <textarea class="form-control" id="mytextarea" name="description" rows="4">{{ old('description',$product->description) }}</textarea>
                            <label class="form-label" for="mytextarea">Description</label>
                        </div>

                        <!-- Checkbox -->
                        <div class="form-outline row">
                           
                            <div class="col">
                                Old image
                                <img src="{{asset('storage/products/'.$product->image)}}" alt="">
                            </div>
                            <div class="col"><label class="form-label" for="customFile">Select a image:</label>
                                <input type="file" class="form-control" id="customFile" name="image"
                                    value="{{ old('image') }}" /></div>
                        </div>

                        <!-- Submit button -->
                        <button type="submit" class="mt-2 btn btn-primary w-100">Update Product</button>


                    </form>





                </div>
            </div>
        </div>






    </div>














</x-backend.layouts.master>
