<x-backend.layouts.master>

    <div class="container">
        <div class="row">
            <div class="card"> 
                <div class="d-flex justify-content-between">
                    <div class="breadcrumb">
                        <li><a href="{{ route('product.index') }}">Products</a></li>/

                        <li><a href=""></a></li>
                    </div>

                    <div>
                        {{-- <a href="{{route('product.trash')}}" class="btn btn-outline-secondary">Trash bin</a> --}}
                        <a href="{{ route('product.create') }}" class="btn btn-outline-info">Add a product</a>
                    </div>
                </div>

            </div>

            <div class="card">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <table>
                    <thead>
                        <th>#</th>
                        <th>photo</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><img src="{{ asset('storage/products/' . $product->image) }}" alt="No image"
                                        style="max-width: 40px; max-height:40px;"></td>
                                <td>{{ $product->title }}</td>
                                <td>{{ Str::limit($product->description, 20) }}</td>
                                <td>{{ $product->size }}</td>
                                <td>{{ $product->price }}</td>
                                <th>
                                    <div class="d-flex">
                                        <a href="{{route('product.show',$product->id)}}" class="btn btn-primary btn-sm">View</a>
                                        <a href="{{route('product.edit',$product->id)}}" class="btn btn-warning btn-sm">Edit</a>
                                        <form action="{{route('product.destroy',$product->id)}}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>






    </div>














</x-backend.layouts.master>
