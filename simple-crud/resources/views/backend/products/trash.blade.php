<x-backend.layouts.master>

    <div class="container"> 
        <a href="{{route('product.index')}}" class="btn btn-primary">Go back</a>
        <div class="row">
            <h1>Trash list</h1>

            <div class="card">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <table>
                    <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $product->title }}</td>
                                <td>{{ Str::limit($product->description, 20) }}</td>
                                <td>{{ $product->price }}</td>
                                <th>
                                    <div class="d-flex">
                                        <a href="{{route('product.restore',$product->id)}}" class="btn btn-primary btn-sm">restore</a>
                                        <form action="{{route('product.delete',$product->id)}}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>






    </div>














</x-backend.layouts.master>
