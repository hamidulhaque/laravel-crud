    <x-frontend.layouts.master>

        <div class="container">

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                @foreach ($products as $product)
                    
              

                <div class="col">
                  <div class="card shadow-sm">
                   <img src="{{asset('storage/products/'.$product->image)}}" class="img-fluid" alt="no image" style="max-height: 255px">
        
                    <div class="card-body">
                      <p class="card-text"> {{$product->description}}</p>
                 
                            <div>Size: {{$product->size}}</div>
                            <div>Price: <b>{{$product->price}}</b></div>
 
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                          <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                          <button type="button" class="btn btn-sm btn-outline-secondary">Add to cart</button>
                        </div>
                        <small class="text-muted">{{$product->created_at->diffForHumans()}}</small>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>






        </div>














    </x-frontend.layouts.master>
