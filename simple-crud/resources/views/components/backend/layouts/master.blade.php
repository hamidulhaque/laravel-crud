<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crud</title>
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
</head>

<body>
<x-backend.layouts.partials.header/>



 {{$slot}}



    <script src="{{ asset('frontend/js/bootstrap.bundle.min.js') }}"></script>
    <x-backend.layouts.partials.footer/>
</body>

</html>
