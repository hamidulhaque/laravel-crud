<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' =>'required|max:100',
            'description' =>'required|max:200', 
            'price'=>'required|numeric',
            'image'=>'required|mimes:jpeg,jpg,png,gif|max:10000',
            'size'=>'required',
        ];
    }
}
