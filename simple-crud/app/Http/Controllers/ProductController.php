<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Database\QueryException;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $products=Product::all();
        return view('backend/products/index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/products/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $requestData=$request->all();
        try {


            if ($request->hasFile('image')) {
                $file = $request->image;
                $path = "/app/public/products/";
                $filename = time() . '.' . $file->getClientOriginalExtension();
                Image::make($file)->resize(300, 200)->save(storage_path() . $path . $filename);
                $requestData['image'] = $filename;
            }

            Product::create($requestData);
            return redirect()->route('product.index')->with('message', 'product stored succesfully');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        
        return view('backend/products/view', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('backend/products/edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        try {
            $requestData = $request->all();

            // $product = Product::findOrFail($id);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path() . '/app/public/products/' . $fileName);
                $requestData['image'] = $fileName;
            } else {
                $requestData['image'] = $product->image;
            }

            $product->update($requestData);

            return redirect()->route('product.index')->withMessage('Successfully Updated !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index')->withMessage('Successfully Deleted !');

    }
    public function trash()
    {
        $products = Product::onlyTrashed()->get();
        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)
    {
        Product::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->route('product.trash')->withMessage('Successfully Restored !');
    }

    public function delete($id)
    {
        Product::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        return redirect()->route('product.trash')->withMessage('Deleted Successfully');
    }








}
