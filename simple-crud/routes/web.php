<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use Faker\Guesser\Name;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('welcome');



require __DIR__ . '/auth.php';
Route::prefix('/admin')->middleware(['auth'])->group(function () {

    Route::get('/', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/deleted-products', [ProductController::class, 'trash'])->name('product.trash');
    Route::get('/deleted-products/{id}/restore', [ProductController::class, 'restore'])->name('product.restore');
    Route::delete('/deleted-products/{id}', [ProductController::class, 'delete'])->name('product.delete');

    Route::resource('product', ProductController::class);
});
